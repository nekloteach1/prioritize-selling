<?php
class Neklo_PrioritizeSelling_Model_Resource_Prioritizeselling_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('neklo_prioritizeselling/prioritizeselling');
    }
    
    public function getJoinedCollection($storeId = 0)
    {
        /** add particular attribute code to this array */
        $productAttributes = array('name', 'url_path');
        foreach ($productAttributes as $attributeCode) {
            $alias     = $attributeCode . '_table';
            $alias2     = $attributeCode . '_tables';
            $attribute = Mage::getSingleton('eav/config')
                ->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
        
            /** Adding eav attribute value */
            $ifNullDefault = $this->getConnection()
                ->getIfNullSql($alias2.'.value', $alias.'.value');
            $this->getSelect()
                ->joinLeft(
                    array($alias => $attribute->getBackendTable()),
                    "main_table.product = $alias.entity_id AND $alias.attribute_id={$attribute->getId()} AND $alias.store_id=0",
                    array('default_'.$attributeCode => 'value'))
                ->joinLeft(
                    array($alias2 => $attribute->getBackendTable()),
                    "main_table.product = $alias2.entity_id AND $alias2.attribute_id={$attribute->getId()} AND $alias2.store_id=$storeId",
                    array('store_'.$attributeCode => 'value','value' =>$ifNullDefault))
            ;
        }
        $customerAttributes = array('lastname', 'firstname');
        foreach ($customerAttributes as $attributeCode) {
            $alias     = $attributeCode . '_table';
            $attribute = Mage::getSingleton('eav/config')
                ->getAttribute('customer', $attributeCode);
           
           $this->getSelect()
               ->joinLeft(
                   array($alias => $attribute->getBackendTable()),
                   "main_table.customer = $alias.entity_id AND $alias.attribute_id={$attribute->getId()}",
                   array($attributeCode => 'value')
            );
        }
        return $this;
    }
}
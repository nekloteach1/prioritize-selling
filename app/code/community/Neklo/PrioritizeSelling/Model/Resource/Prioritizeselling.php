<?php

class Neklo_PrioritizeSelling_Model_Resource_Prioritizeselling extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('neklo_prioritizeselling/prioritizeselling', 'id');
    }
}
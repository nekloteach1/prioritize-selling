<?php

class Neklo_PrioritizeSelling_Model_Source_Config
{
    
    public function toOptionArray()
    {
        return array(
            array('value' => 1,
                  'label' => Mage::helper('neklo_prioritizeselling')->__('Make product out of stock until all pre-order completed')),
            array('value' => 2,
                  'label' => Mage::helper('neklo_prioritizeselling')->__('If quantity more than pre-orders make product status In Stock')),
            array('value' => 3,
                  'label' => Mage::helper('neklo_prioritizeselling')->__('Just notify customer about the appearance of products')),
        );
    }
    
    
}
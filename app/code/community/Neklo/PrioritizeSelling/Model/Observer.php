<?php

class Neklo_PrioritizeSelling_Model_Observer
{
    public function checkOrderStatus($observer)
    {
        $customerEmail = $observer->getOrder()->getCustomerEmail();
        if ($observer->getOrder()->getStatus() == 'processing') {
            $products = $observer->getOrder()->getAllItems();
            Mage::helper('neklo_prioritizeselling')
                ->deletePrioritizeRows($products, $customerEmail);
        }
    }
    
    public function checkConfigurableProductStatus($observer)
    {
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $actionName = Mage::app()->getRequest()->getActionName();
        if ($controllerName != 'product' AND $actionName != 'view') {
            return;
        }
        $parentId = $observer->getProduct()->getParentId();
        if ($parentId == null) {
            return;
        }
        $configSetting = Mage::helper('neklo_prioritizeselling')
            ->getConfigSettings();
        if ($observer->getProduct()->getEntityId() == $parentId) {
            return;
        }
        if ($configSetting == 1) {
            if ($observer->getProduct()->getIsSalable() == 1) {
                $productId = $observer->getProduct()->getEntityId();
                if (Mage::getSingleton('core/session')->getPrioritizeProduct()
                    == $productId
                ) {
                    return;
                }
                $checkProducts = Mage::helper('neklo_prioritizeselling')
                    ->getCountPrioritizeProducts($productId);
                if ($checkProducts == 0) {
                    return;
                }
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $customerId = Mage::getSingleton('customer/session')
                        ->getCustomer()
                        ->getId();
                    $expect = Mage::helper('neklo_prioritizeselling')
                        ->getCountPrioritizeProductsByCustomer(
                            $productId, $customerId
                        );
                    if ($expect > 0) {
                        return;
                    }
                } elseif (Mage::app()->getRequest()->getParam('url_param')
                    != null
                ) {
                    $urlParam = Mage::app()->getRequest()
                        ->getParam('url_param');
                    $expect = Mage::helper('neklo_prioritizeselling')
                        ->getCountPrioritizeProductsByUrlParam(
                            $productId, $urlParam
                        );
                    if ($expect > 0) {
                        Mage::getSingleton('core/session')
                            ->setPrioritizeProduct($productId);
                        return;
                    }
                }
                $observer->getProduct()->setIsInStock(0);
                $observer->getProduct()->setIsSalable(0);
            }
        }
        if ($configSetting == 2) {
            if ($observer->getEvent()->getData('product')->getIsSalable()
                == 1
            ) {
                $productId = $observer->getData('product')->getData(
                    'entity_id'
                );
                if (Mage::getSingleton('core/session')->getPrioritizeProduct()
                    == $productId
                ) {
                    return;
                }
                $checkProducts = Mage::helper('neklo_prioritizeselling')
                    ->getCountPrioritizeProducts($productId);
                if ($checkProducts == 0) {
                    return;
                }
                $productQty = Mage::getModel('cataloginventory/stock_item')
                    ->loadByProduct($productId)->getQty();
                
                if ($productQty > $checkProducts) {
                    $observer->getEvent()
                        ->getData('product')
                        ->setQty(
                            $observer->getEvent()
                                ->getData('product')
                                ->getQty() - $checkProducts
                        );
                    return;
                }
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $customerId = Mage::getSingleton('customer/session')
                        ->getCustomer()
                        ->getId();
                    $expect = Mage::helper('neklo_prioritizeselling')
                        ->getCountPrioritizeProductsByCustomer(
                            $productId, $customerId
                        );
                    if ($expect > 0) {
                        return;
                    }
                } elseif (Mage::app()->getRequest()->getParam('url_param')
                    != null
                ) {
                    $urlParam = Mage::app()->getRequest()
                        ->getParam('url_param');
                    $expect = Mage::helper('neklo_prioritizeselling')
                        ->getCountPrioritizeProductsByUrlParam(
                            $productId, $urlParam
                        );
                    if ($expect > 0) {
                        Mage::getSingleton('core/session')
                            ->setPrioritizeProduct($productId);
                        return;
                    }
                }
                $observer->getProduct()->setIsInStock(0);
                $observer->getProduct()->setIsSalable(0);
            }
        }
        if ($configSetting == 3) {
            return;
        }
    }
    
    public function checkProductStatus($observer)
    {
        if (Mage::app()->getRequest()->getControllerName() != 'product'
            AND Mage::app()->getRequest()->getActionName() != 'view'
        ) {
            return;
        }
        if ($observer->getEvent()->getData('product')->getTypeId()
            == 'configurable' AND
            $observer->getEvent()->getData('product')->getIsSalable() == 0
        ) {
            Mage::register(
                'product_id',
                $observer->getEvent()->getData('product')->getEntityId()
            );
            Mage::register('flag', 1);
            return;
        }
        if (Mage::getSingleton('core/session')->getPrioritizeProduct()
            == $observer->getEvent()->getData('product')->getEntityId()
        ) {
            return;
        }
        
        if ($observer->getEvent()->getData('product')->getTypeId() == 'simple'
            AND $observer->getEvent()->getData('product')->getIsSalable() == 0
        ) {
            Mage::register(
                'product_id',
                $observer->getEvent()->getData('product')->getEntityId()
            );
            Mage::register('flag', 1);
            return;
        }
        $configSetting = Mage::helper('neklo_prioritizeselling')
            ->getConfigSettings();
        if ($configSetting == 1) {
            if ($observer->getEvent()->getData('product')->getIsSalable()
                == 1
            ) {
                $productId = $observer->getData('product')->getData(
                    'entity_id'
                );
                $checkProducts = Mage::helper('neklo_prioritizeselling')
                    ->getCountPrioritizeProducts($productId);
                if ($checkProducts == 0) {
                    return;
                }
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $customerId = Mage::getSingleton('customer/session')
                        ->getCustomer()
                        ->getId();
                    $expect = Mage::helper('neklo_prioritizeselling')
                        ->getCountPrioritizeProductsByCustomer(
                            $productId, $customerId
                        );
                    if ($expect > 0) {
                        return;
                    }
                } elseif (Mage::app()->getRequest()->getParam('url_param')
                    != null
                ) {
                    $urlParam = Mage::app()->getRequest()
                        ->getParam('url_param');
                    $expect = Mage::helper('neklo_prioritizeselling')
                        ->getCountPrioritizeProductsByUrlParam(
                            $productId, $urlParam
                        );
                    if ($expect > 0) {
                        Mage::getSingleton('core/session')
                            ->setPrioritizeProduct($productId);
                        return;
                    }
                }
                $observer->getEvent()->getData('product')->setIsSalable(0);
                Mage::register('flag', 1);
                Mage::register(
                    'product_id',
                    $observer->getEvent()->getData('product')->getEntityId()
                );
            }
        }
        if ($configSetting == 2) {
            if ($observer->getEvent()->getData('product')->getIsSalable()
                == 1
            ) {
                $productId = $observer->getData('product')->getData(
                    'entity_id'
                );
                $checkProducts = Mage::helper('neklo_prioritizeselling')
                    ->getCountPrioritizeProducts($productId);
                if ($checkProducts == 0) {
                    return;
                }
                $productQty = Mage::getModel('cataloginventory/stock_item')
                    ->loadByProduct($productId)->getQty();;
                if ($productQty > $checkProducts) {
                    $observer->getEvent()
                        ->getData('product')
                        ->setQty(
                            $observer->getEvent()
                                ->getData('product')
                                ->getQty() - $checkProducts
                        );
                    return;
                }
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $customerId = Mage::getSingleton('customer/session')
                        ->getCustomer()
                        ->getId();
                    $expect = Mage::helper('neklo_prioritizeselling')
                        ->getCountPrioritizeProductsByCustomer(
                            $productId, $customerId
                        );
                    if ($expect > 0) {
                        return;
                    }
                } elseif (Mage::app()->getRequest()->getParam('url_param')
                    != null
                ) {
                    $urlParam = Mage::app()->getRequest()
                        ->getParam('url_param');
                    $expect = Mage::helper('neklo_prioritizeselling')
                        ->getCountPrioritizeProductsByUrlParam(
                            $productId, $urlParam
                        );
                    
                    if ($expect > 0) {
                        Mage::getSingleton('core/session')
                            ->setPrioritizeProduct($productId);
                        return;
                    }
                }
                $observer->getEvent()->getData('product')->setIsSalable(0);
                Mage::register('flag', 1);
                Mage::register(
                    'product_id',
                    $observer->getEvent()->getData('product')->getEntityId()
                );
            }
        }
        if ($configSetting == 3) {
            return;
        }
    }
    
    public function checkChangeProductStatus($observer)
    {
        $productId = $observer->getEvent()->getData('item')->getData(
            'product_id'
        );
        if ($observer->getEvent()->getData('item')->getData('is_in_stock') == 1
            AND $observer->getEvent()->getData('item')->getOrigData(
                'is_in_stock'
            )
            == 0
        ) {
            $productIds = Mage::getModel('catalog/product_type_configurable')
                ->getParentIdsByChild($productId);
            $productIds[] = $productId;
            unset($productId);
            Mage::helper('neklo_prioritizeselling')
                ->sendEmail($productIds);
            return;
        }
        if ($observer->getEvent()->getData('item')->getData('is_in_stock') == 0
            AND $observer->getEvent()->getData('item')->getOrigData(
                'is_in_stock'
            )
            == 1
        ) {
            $preOrders = Mage::getModel(
                'neklo_prioritizeselling/prioritizeselling'
            )->getCollection()
                ->addFieldToFilter(
                    'product', array('eq' => $productId)
                );
            foreach ($preOrders as $item) {
                $item->setIsActive(0);
                $item->setIsSend(0);
                $item->save();
            }
        }
    }
    
    public function changeStatusOnProductList($observer)
    {
        $productsCollection = $observer->getCollection()->getItems();
        $configSetting = Mage::helper('neklo_prioritizeselling')
            ->getConfigSettings();
        foreach ($productsCollection as $product) {
            if ($configSetting == 1) {
                $checkProducts = Mage::helper('neklo_prioritizeselling')
                    ->getCountPrioritizeProducts($product->getEntityId());
                if ($checkProducts == 0) {
                    continue;
                }
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $customerId = Mage::getSingleton('customer/session')
                        ->getCustomer()
                        ->getId();
                    $expect = Mage::helper('neklo_prioritizeselling')
                        ->getCountPrioritizeProductsByCustomer(
                            $product->getEntityId(), $customerId
                        );
                    if ($expect > 0) {
                        continue;
                    }
                }
                
                if (Mage::getSingleton('core/session')->getPrioritizeProduct()
                    == $product->getEntityId()
                ) {
                    continue;
                }
                $product->setIsSalable(0);
                $product->getStockItem()->setIsInStock(0);
            }
            if ($configSetting == 2) {
                if ($product->getIsSalable() == 0) {
                    continue;
                }
                $checkProducts = Mage::helper('neklo_prioritizeselling')
                    ->getCountPrioritizeProducts($product->getEntityId());
                
                $qty = Mage::getModel('cataloginventory/stock_item')
                    ->loadByProduct($product->getEntityId())->getQty();
                if ($checkProducts == 0) {
                    continue;
                }
                if ($qty > $checkProducts) {
                    continue;
                }
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $customerId = Mage::getSingleton('customer/session')
                        ->getCustomer()
                        ->getId();
                    $expect = $expect = Mage::helper('neklo_prioritizeselling')
                        ->getCountPrioritizeProductsByCustomer(
                            $product->getEntityId(), $customerId
                        );
                    if ($expect > 0) {
                        continue;
                    }
                }
                
                if (Mage::getSingleton('core/session')->getPrioritizeProduct()
                    == $product->getEntityId()
                ) {
                    continue;
                }
                $product->setIsSalable(0);
                $product->getStockItem()->setIsInStock(0);
            }
        }
    }
}

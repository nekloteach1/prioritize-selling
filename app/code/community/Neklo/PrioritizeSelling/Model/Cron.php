<?php

class Neklo_PrioritizeSelling_Model_Cron
{
    public function neklo_prioritizeselling_check_date()
    {
        $preOrders = Mage::getModel('neklo_prioritizeselling/prioritizeselling')->getCollection();
    
        foreach ($preOrders as $item){
            $date = new DateTime($item->getReceiptDate());
            $currentDate = new DateTime( Mage::getModel('core/date')->gmtDate('Y-m-d'));
            $interval = $date->diff($currentDate);
            $configSetting = Mage::getStoreConfig(
                'neklo_prioritizeselling/neklo_prioritizeselling_group/neklo_prioritizeselling_input',
                Mage::app()->getStore()
            );
            if($item->getIsActive() == 1 AND $item->getIsSend() == 1) {
                if ($interval->format('%R') == '+' AND $interval->format('%a') >= $configSetting) {
                    $item->delete();
                }
            }
        }
    }
}

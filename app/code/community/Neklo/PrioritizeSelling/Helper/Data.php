<?php
class Neklo_PrioritizeSelling_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function deletePrioritizeRows($products, $customerEmail)
    {
        foreach ($products as $product) {
            $productId = $product->getProductId();
            $rowsToDelete =  Mage::getModel('neklo_prioritizeselling/prioritizeselling')->getCollection()
                ->addFieldToFilter(
                    'product', array('eq' => $productId)
                )
                ->addFieldToFilter(
                    'email',
                    array('eq' => $customerEmail)
                );
            foreach ($rowsToDelete as $row) {
                $row->delete();
            }
        }
    }
    public function getConfigSettings()
    {
       return Mage::getStoreConfig(
            'neklo_prioritizeselling/neklo_prioritizeselling_group/neklo_prioritizeselling_select',
            Mage::app()->getStore()
       );
    }
    
    public function getCountPrioritizeProducts($productId)
    {
        return Mage::getModel('neklo_prioritizeselling/prioritizeselling')
            ->getCollection()
            ->addFieldToFilter('product', array('eq' => $productId))
            ->getSize();
    }
    
    public function getCountPrioritizeProductsByCustomer($productId, $customerId)
    {
       return Mage::getModel('neklo_prioritizeselling/prioritizeselling')->getCollection()
            ->addFieldToFilter(
                'product', array('eq' => $productId)
            )
            ->addFieldToFilter(
                'customer', array('eq' => $customerId)
            )
            ->getSize();
    }
    
    public function getCountPrioritizeProductsByUrlParam($productId, $urlParam)
    {
        return Mage::getModel('neklo_prioritizeselling/prioritizeselling')->getCollection()
            ->addFieldToFilter(
                'product', array('eq' => $productId)
            )
            ->addFieldToFilter(
                'url_param',
                array('eq' => $urlParam)
            )
            ->getSize()
        ;
    }
    
    public function sendEmail($productIds)
    {
        foreach ($productIds as $productId) {
            $preOrders = Mage::getModel(
                'neklo_prioritizeselling/prioritizeselling'
            )->getCollection()
                ->addFieldToFilter(
                    'product', array('eq' => $productId)
                );
            foreach ($preOrders as $item) {
                if($item->getIsActive() == 1 AND $item->getIsSend() == 1){
                    continue;
                }
                $productCollection = Mage::getModel('catalog/product')
                    ->setStoreId(1)
                    ->load($productId);
                if ($item->getCustomer() != 0) {
                    $product_url = Mage::getUrl('', array('_store' => 1))
                        . $productCollection->getUrlPath();
                } else {
                    $product_url = Mage::getUrl('', array('_store' => 1))
                        . $productCollection->getUrlPath() . '?url_param='
                        . $item->getUrlParam();
                }
                $productName = $productCollection->getName();
                $template_id = 'neklo_prioritizeselling_email';
                $email_to = $item->getEmail();
                $customer_name = 'John Doe';
                $email_template = Mage::getModel('core/email_template')
                    ->loadDefault($template_id);
                $email_template_variables = array(
                    'product' => $productName,
                    'product_url' => $product_url,
                );
                $sender_name = Mage::getStoreConfig(
                    Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME
                );
                $sender_email = Mage::getStoreConfig(
                    'trans_email/ident_general/email'
                );
                $email_template->setSenderName('Test');
                $email_template->setTemplateSubject('TEST');
                $email_template->setSenderEmail($sender_email);
                $email_template->send(
                    $email_to, $customer_name, $email_template_variables
                );
                $item->setIsSend(1);
                $item->setReceiptDate(
                    Mage::getModel('core/date')->gmtDate('Y-m-d')
                );
                $item->setIsActive(1);
                $item->save();
            
            }
        }
    }
    
}
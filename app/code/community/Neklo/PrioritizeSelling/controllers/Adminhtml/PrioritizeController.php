<?php
class Neklo_PrioritizeSelling_Adminhtml_PrioritizeController
    extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()->_title($this->__('Prioritize Selling'));;
        $this->_addContent(
            $this->getLayout()->createBlock('neklo_prioritizeselling/adminhtml_prioritize')
        );
        $this->renderLayout();
    }
    
    public function massDeleteAction()
    {
        $Ids = $this->getRequest()->getParam('neklo_prioritizeselling');
        
        if(!is_array($Ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select checkbox'));
        } else {
            try {
                $model = Mage::getModel('neklo_prioritizeselling/prioritizeselling');
                foreach ($Ids as $adId) {
                    $model->load($adId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Total of %d record(s) were deleted.', count($Ids)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed(
            'prioritize/neklo_prioritizeselling'
        );
    }
}
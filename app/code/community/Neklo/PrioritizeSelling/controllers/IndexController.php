<?php

class Neklo_PrioritizeSelling_IndexController
    extends Mage_Core_Controller_Front_Action
{
    public function ajaxsaveAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            
            $model = Mage::getModel('neklo_prioritizeselling/prioritizeselling');
            $email = $this->getRequest()->getParam('email');
            $productId = $this->getRequest()->getParam('product_id');
            $checkExist = $model->getCollection()
                ->addFieldToFilter(
                    'product', array('eq' => $productId)
                )
                ->addFieldToFilter(
                    'email', array('eq' => $email)
                )
                ->getSize()
            ;
            if($checkExist > 0){
                echo json_encode(array('result' => 'You are already added this product.'));
                return;
            }
            $model->setProduct($productId);
            $model->setEmail($email);
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $customerId = Mage::getSingleton('customer/session')
                    ->getCustomer()
                    ->getId();
                $model->setCustomer($customerId);
                $model->setUrlParam('');
            } else {
                $model->setUrlParam(md5($email . $productId));
            }
            $model->setIsActive(0);
            $model->setIsSend(0);
            $model->save();
            echo json_encode(array('result' => 'Your request was successfully accepted'));
        } else {
            $this->_redirect("/");
        }
    }
    
    public function expectedAction()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->loadLayout();
            $this->renderLayout();
        } else{
            $this->_redirect("/");
        }
        
    }
}

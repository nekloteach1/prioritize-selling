<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();


$installer->run("
create table `{$this->getTable('neklo_prioritizeselling/prioritizeselling')}`
(
	id int auto_increment
		primary key,
	customer int(11) not null,
	email varchar(255) not null,
	product int(11) not null,
	is_send char(1) not null,
	is_active char(1) not null,
	url_param varchar(32) not null,
	receipt_date DATE not null
)
;

");

$installer->endSetup();


<?php

class Neklo_PrioritizeSelling_Block_ExpectedProducts
    extends Mage_Core_Block_Template
{
    protected function getExpectedProducts($status)
    {
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer()->getId();
        }else{
            return false;
        }
        if($customer) {
            $expectedProducts = Mage::getModel('neklo_prioritizeselling/prioritizeselling')
                ->getCollection()
                ->addFieldToFilter('customer', array('eq' => $customer))
                ->addFieldToFilter('is_active', array('eq' => $status))
                ->getJoinedCollection(Mage::app()->getStore()->getStoreId())
            ;
            return $expectedProducts;
        }
        
    }
}
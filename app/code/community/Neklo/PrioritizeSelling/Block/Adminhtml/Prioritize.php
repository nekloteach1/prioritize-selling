<?php
class Neklo_PrioritizeSelling_Block_Adminhtml_Prioritize
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_prioritize';
        $this->_blockGroup = 'neklo_prioritizeselling';
        $this->_headerText = Mage::helper('neklo_prioritizeselling')->__('Prioritize Selling');
        parent::__construct();
        $this->_removeButton('add');
    }
}
<?php

class Neklo_PrioritizeSelling_Block_Adminhtml_Prioritize_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('id');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
    }
    
    protected function _prepareCollection()
    {
        $checkProducts = Mage::getModel(
            'neklo_prioritizeselling/prioritizeselling'
        )->getCollection()->getJoinedCollection();
        $this->setCollection($checkProducts);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn(
            'customer', array(
                'header' => Mage::helper('neklo_prioritizeselling')->__(
                    'Customer'
                ),
                'align'  => 'left',
                'renderer' => 'Neklo_PrioritizeSelling_Block_Adminhtml_Prioritize_Renderer_Customer'
            )
        );
        $this->addColumn(
            'email', array(
                'header' => Mage::helper('neklo_prioritizeselling')->__(
                    'email'
                ),
                'align'  => 'left',
                'index'  => 'email',
            )
        );
        $this->addColumn(
            'default_name', array(
                'header' => Mage::helper('neklo_prioritizeselling')->__(
                    'Product'
                ),
                'align'  => 'left',
                'index'  => 'default_name',
            )
        );
        return parent::_prepareColumns();
    }
    
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName(
            'neklo_prioritizeselling'
        );
        
        $this->getMassactionBlock()
            ->addItem(
                'delete', array(
                    'label' => Mage::helper('neklo_prioritizeselling')->__('Delete'),
                    'url'   => $this->getUrl('*/*/massDelete'),
                    )
            );
    }
}

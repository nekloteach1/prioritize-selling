<?php

class Neklo_PrioritizeSelling_Block_Adminhtml_Prioritize_Renderer_Customer
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        if($row->getCustomer() == 0){
            return 'Guest';
        }
        return $row->getFirstname()." ".$row->getLastname();
    }
}
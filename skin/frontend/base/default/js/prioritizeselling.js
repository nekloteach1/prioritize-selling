function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function sendForm(ajaxurl,imageurl) {
    event.preventDefault();
    if (!validateEmail(jQuery('#email').val())) {
        document.getElementById('validate_email').innerHTML = 'Invalid Email';
        return;
    }
    msg = new Object();
    msg.email = jQuery('#email').val();
    msg.product_id = jQuery('#product_id').val();
    document.getElementById('prioritize').innerHTML = "<img src='" + imageurl + "' height = '50px' width = 'auto' > ";
    jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: msg,
        success: function (data) {
            response = jQuery.parseJSON(data);
            document.getElementById('prioritize').innerHTML = response.result;
        },
        error: function (xhr, str) {
        }
    });
}